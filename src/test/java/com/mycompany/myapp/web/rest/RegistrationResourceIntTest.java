package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.AbstractCassandraTest;
import com.mycompany.myapp.HshrcApp;

import com.mycompany.myapp.domain.Registration;
import com.mycompany.myapp.repository.RegistrationRepository;
import com.mycompany.myapp.service.RegistrationService;
import com.mycompany.myapp.service.dto.RegistrationDTO;
import com.mycompany.myapp.service.mapper.RegistrationMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RegistrationResource REST controller.
 *
 * @see RegistrationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HshrcApp.class)
public class RegistrationResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final String DEFAULT_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_CATEGOTY = "AAAAAAAAAA";
    private static final String UPDATED_CATEGOTY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_RELATION_WITH_PATIENT = "AAAAAAAAAA";
    private static final String UPDATED_RELATION_WITH_PATIENT = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_WHO_BOUGHT_PATIENT = "AAAAAAAAAA";
    private static final String UPDATED_NAME_WHO_BOUGHT_PATIENT = "BBBBBBBBBB";

    private static final String DEFAULT_SPECIALITY = "AAAAAAAAAA";
    private static final String UPDATED_SPECIALITY = "BBBBBBBBBB";

    private static final String DEFAULT_DOCTOR_TO_VISIT = "AAAAAAAAAA";
    private static final String UPDATED_DOCTOR_TO_VISIT = "BBBBBBBBBB";

    private static final String DEFAULT_REFREL_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REFREL_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_REFERED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REFERED_BY = "BBBBBBBBBB";

    private static final Integer DEFAULT_REGISTRATION_FEE = 1;
    private static final Integer UPDATED_REGISTRATION_FEE = 2;

    private static final Integer DEFAULT_UID = 1;
    private static final Integer UPDATED_UID = 2;

    private static final Long DEFAULT_PHONE_NUMBER = 1L;
    private static final Long UPDATED_PHONE_NUMBER = 2L;

    private static final String DEFAULT_UID_RANDOM = "AAAAAAAAAA";
    private static final String UPDATED_UID_RANDOM = "BBBBBBBBBB";

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationMapper registrationMapper;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restRegistrationMockMvc;

    private Registration registration;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RegistrationResource registrationResource = new RegistrationResource(registrationService);
        this.restRegistrationMockMvc = MockMvcBuilders.standaloneSetup(registrationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Registration createEntity() {
        Registration registration = new Registration()
            .first_name(DEFAULT_FIRST_NAME)
            .last_name(DEFAULT_LAST_NAME)
            .age(DEFAULT_AGE)
            .gender(DEFAULT_GENDER)
            .categoty(DEFAULT_CATEGOTY)
            .state(DEFAULT_STATE)
            .district(DEFAULT_DISTRICT)
            .address(DEFAULT_ADDRESS)
            .relation_with_patient(DEFAULT_RELATION_WITH_PATIENT)
            .name_who_bought_patient(DEFAULT_NAME_WHO_BOUGHT_PATIENT)
            .speciality(DEFAULT_SPECIALITY)
            .doctor_to_visit(DEFAULT_DOCTOR_TO_VISIT)
            .refrel_type(DEFAULT_REFREL_TYPE)
            .refered_by(DEFAULT_REFERED_BY)
            .registration_fee(DEFAULT_REGISTRATION_FEE)
            .uid(DEFAULT_UID)
            .phone_number(DEFAULT_PHONE_NUMBER)
            .uid_random(DEFAULT_UID_RANDOM);
        return registration;
    }

    @Before
    public void initTest() {
        registrationRepository.deleteAll();
        registration = createEntity();
    }

    @Test
    public void createRegistration() throws Exception {
        int databaseSizeBeforeCreate = registrationRepository.findAll().size();

        // Create the Registration
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);
        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isCreated());

        // Validate the Registration in the database
        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeCreate + 1);
        Registration testRegistration = registrationList.get(registrationList.size() - 1);
        assertThat(testRegistration.getFirst_name()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testRegistration.getLast_name()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testRegistration.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testRegistration.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testRegistration.getCategoty()).isEqualTo(DEFAULT_CATEGOTY);
        assertThat(testRegistration.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testRegistration.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testRegistration.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testRegistration.getRelation_with_patient()).isEqualTo(DEFAULT_RELATION_WITH_PATIENT);
        assertThat(testRegistration.getName_who_bought_patient()).isEqualTo(DEFAULT_NAME_WHO_BOUGHT_PATIENT);
        assertThat(testRegistration.getSpeciality()).isEqualTo(DEFAULT_SPECIALITY);
        assertThat(testRegistration.getDoctor_to_visit()).isEqualTo(DEFAULT_DOCTOR_TO_VISIT);
        assertThat(testRegistration.getRefrel_type()).isEqualTo(DEFAULT_REFREL_TYPE);
        assertThat(testRegistration.getRefered_by()).isEqualTo(DEFAULT_REFERED_BY);
        assertThat(testRegistration.getRegistration_fee()).isEqualTo(DEFAULT_REGISTRATION_FEE);
        assertThat(testRegistration.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testRegistration.getPhone_number()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testRegistration.getUid_random()).isEqualTo(DEFAULT_UID_RANDOM);
    }

    @Test
    public void createRegistrationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = registrationRepository.findAll().size();

        // Create the Registration with an existing ID
        registration.setId(UUID.randomUUID());
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkFirst_nameIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setFirst_name(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setAge(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setGender(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkCategotyIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setCategoty(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setState(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkDistrictIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setDistrict(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setAddress(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkRegistration_feeIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setRegistration_fee(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkUidIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setUid(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkPhone_numberIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationRepository.findAll().size();
        // set the field null
        registration.setPhone_number(null);

        // Create the Registration, which fails.
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        restRegistrationMockMvc.perform(post("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isBadRequest());

        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllRegistrations() throws Exception {
        // Initialize the database
        registrationRepository.save(registration);

        // Get all the registrationList
        restRegistrationMockMvc.perform(get("/api/registrations"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registration.getId().toString())))
            .andExpect(jsonPath("$.[*].first_name").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].last_name").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].categoty").value(hasItem(DEFAULT_CATEGOTY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].relation_with_patient").value(hasItem(DEFAULT_RELATION_WITH_PATIENT.toString())))
            .andExpect(jsonPath("$.[*].name_who_bought_patient").value(hasItem(DEFAULT_NAME_WHO_BOUGHT_PATIENT.toString())))
            .andExpect(jsonPath("$.[*].speciality").value(hasItem(DEFAULT_SPECIALITY.toString())))
            .andExpect(jsonPath("$.[*].doctor_to_visit").value(hasItem(DEFAULT_DOCTOR_TO_VISIT.toString())))
            .andExpect(jsonPath("$.[*].refrel_type").value(hasItem(DEFAULT_REFREL_TYPE.toString())))
            .andExpect(jsonPath("$.[*].refered_by").value(hasItem(DEFAULT_REFERED_BY.toString())))
            .andExpect(jsonPath("$.[*].registration_fee").value(hasItem(DEFAULT_REGISTRATION_FEE)))
            .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID)))
            .andExpect(jsonPath("$.[*].phone_number").value(hasItem(DEFAULT_PHONE_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].uid_random").value(hasItem(DEFAULT_UID_RANDOM.toString())));
    }

    @Test
    public void getRegistration() throws Exception {
        // Initialize the database
        registrationRepository.save(registration);

        // Get the registration
        restRegistrationMockMvc.perform(get("/api/registrations/{id}", registration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(registration.getId().toString()))
            .andExpect(jsonPath("$.first_name").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.last_name").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.categoty").value(DEFAULT_CATEGOTY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.relation_with_patient").value(DEFAULT_RELATION_WITH_PATIENT.toString()))
            .andExpect(jsonPath("$.name_who_bought_patient").value(DEFAULT_NAME_WHO_BOUGHT_PATIENT.toString()))
            .andExpect(jsonPath("$.speciality").value(DEFAULT_SPECIALITY.toString()))
            .andExpect(jsonPath("$.doctor_to_visit").value(DEFAULT_DOCTOR_TO_VISIT.toString()))
            .andExpect(jsonPath("$.refrel_type").value(DEFAULT_REFREL_TYPE.toString()))
            .andExpect(jsonPath("$.refered_by").value(DEFAULT_REFERED_BY.toString()))
            .andExpect(jsonPath("$.registration_fee").value(DEFAULT_REGISTRATION_FEE))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID))
            .andExpect(jsonPath("$.phone_number").value(DEFAULT_PHONE_NUMBER.intValue()))
            .andExpect(jsonPath("$.uid_random").value(DEFAULT_UID_RANDOM.toString()));
    }

    @Test
    public void getNonExistingRegistration() throws Exception {
        // Get the registration
        restRegistrationMockMvc.perform(get("/api/registrations/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateRegistration() throws Exception {
        // Initialize the database
        registrationRepository.save(registration);
        int databaseSizeBeforeUpdate = registrationRepository.findAll().size();

        // Update the registration
        Registration updatedRegistration = registrationRepository.findOne(registration.getId());
        updatedRegistration
            .first_name(UPDATED_FIRST_NAME)
            .last_name(UPDATED_LAST_NAME)
            .age(UPDATED_AGE)
            .gender(UPDATED_GENDER)
            .categoty(UPDATED_CATEGOTY)
            .state(UPDATED_STATE)
            .district(UPDATED_DISTRICT)
            .address(UPDATED_ADDRESS)
            .relation_with_patient(UPDATED_RELATION_WITH_PATIENT)
            .name_who_bought_patient(UPDATED_NAME_WHO_BOUGHT_PATIENT)
            .speciality(UPDATED_SPECIALITY)
            .doctor_to_visit(UPDATED_DOCTOR_TO_VISIT)
            .refrel_type(UPDATED_REFREL_TYPE)
            .refered_by(UPDATED_REFERED_BY)
            .registration_fee(UPDATED_REGISTRATION_FEE)
            .uid(UPDATED_UID)
            .phone_number(UPDATED_PHONE_NUMBER)
            .uid_random(UPDATED_UID_RANDOM);
        RegistrationDTO registrationDTO = registrationMapper.toDto(updatedRegistration);

        restRegistrationMockMvc.perform(put("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isOk());

        // Validate the Registration in the database
        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeUpdate);
        Registration testRegistration = registrationList.get(registrationList.size() - 1);
        assertThat(testRegistration.getFirst_name()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testRegistration.getLast_name()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testRegistration.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testRegistration.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testRegistration.getCategoty()).isEqualTo(UPDATED_CATEGOTY);
        assertThat(testRegistration.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testRegistration.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testRegistration.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testRegistration.getRelation_with_patient()).isEqualTo(UPDATED_RELATION_WITH_PATIENT);
        assertThat(testRegistration.getName_who_bought_patient()).isEqualTo(UPDATED_NAME_WHO_BOUGHT_PATIENT);
        assertThat(testRegistration.getSpeciality()).isEqualTo(UPDATED_SPECIALITY);
        assertThat(testRegistration.getDoctor_to_visit()).isEqualTo(UPDATED_DOCTOR_TO_VISIT);
        assertThat(testRegistration.getRefrel_type()).isEqualTo(UPDATED_REFREL_TYPE);
        assertThat(testRegistration.getRefered_by()).isEqualTo(UPDATED_REFERED_BY);
        assertThat(testRegistration.getRegistration_fee()).isEqualTo(UPDATED_REGISTRATION_FEE);
        assertThat(testRegistration.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testRegistration.getPhone_number()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testRegistration.getUid_random()).isEqualTo(UPDATED_UID_RANDOM);
    }

    @Test
    public void updateNonExistingRegistration() throws Exception {
        int databaseSizeBeforeUpdate = registrationRepository.findAll().size();

        // Create the Registration
        RegistrationDTO registrationDTO = registrationMapper.toDto(registration);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRegistrationMockMvc.perform(put("/api/registrations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationDTO)))
            .andExpect(status().isCreated());

        // Validate the Registration in the database
        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteRegistration() throws Exception {
        // Initialize the database
        registrationRepository.save(registration);
        int databaseSizeBeforeDelete = registrationRepository.findAll().size();

        // Get the registration
        restRegistrationMockMvc.perform(delete("/api/registrations/{id}", registration.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Registration> registrationList = registrationRepository.findAll();
        assertThat(registrationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Registration.class);
        Registration registration1 = new Registration();
        registration1.setId(UUID.randomUUID());
        Registration registration2 = new Registration();
        registration2.setId(registration1.getId());
        assertThat(registration1).isEqualTo(registration2);
        registration2.setId(UUID.randomUUID());
        assertThat(registration1).isNotEqualTo(registration2);
        registration1.setId(null);
        assertThat(registration1).isNotEqualTo(registration2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistrationDTO.class);
        RegistrationDTO registrationDTO1 = new RegistrationDTO();
        registrationDTO1.setId(UUID.randomUUID());
        RegistrationDTO registrationDTO2 = new RegistrationDTO();
        assertThat(registrationDTO1).isNotEqualTo(registrationDTO2);
        registrationDTO2.setId(registrationDTO1.getId());
        assertThat(registrationDTO1).isEqualTo(registrationDTO2);
        registrationDTO2.setId(UUID.randomUUID());
        assertThat(registrationDTO1).isNotEqualTo(registrationDTO2);
        registrationDTO1.setId(null);
        assertThat(registrationDTO1).isNotEqualTo(registrationDTO2);
    }
}
