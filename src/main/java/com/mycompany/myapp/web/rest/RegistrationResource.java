package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.RegistrationService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.service.dto.RegistrationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing Registration.
 */
@RestController
@RequestMapping("/api")
public class RegistrationResource {

    private final Logger log = LoggerFactory.getLogger(RegistrationResource.class);

    private static final String ENTITY_NAME = "registration";

    private final RegistrationService registrationService;

    public RegistrationResource(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    /**
     * POST  /registrations : Create a new registration.
     *
     * @param registrationDTO the registrationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new registrationDTO, or with status 400 (Bad Request) if the registration has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/registrations")
    @Timed
    public ResponseEntity<RegistrationDTO> createRegistration(@Valid @RequestBody RegistrationDTO registrationDTO) throws URISyntaxException {
        log.debug("REST request to save Registration : {}", registrationDTO);
        if (registrationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new registration cannot already have an ID")).body(null);
        }
        RegistrationDTO result = registrationService.save(registrationDTO);
        return ResponseEntity.created(new URI("/api/registrations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /registrations : Updates an existing registration.
     *
     * @param registrationDTO the registrationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated registrationDTO,
     * or with status 400 (Bad Request) if the registrationDTO is not valid,
     * or with status 500 (Internal Server Error) if the registrationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/registrations")
    @Timed
    public ResponseEntity<RegistrationDTO> updateRegistration(@Valid @RequestBody RegistrationDTO registrationDTO) throws URISyntaxException {
        log.debug("REST request to update Registration : {}", registrationDTO);
        if (registrationDTO.getId() == null) {
            return createRegistration(registrationDTO);
        }
        RegistrationDTO result = registrationService.save(registrationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, registrationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /registrations : get all the registrations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of registrations in body
     */
    @GetMapping("/registrations")
    @Timed
    public List<RegistrationDTO> getAllRegistrations() {
        log.debug("REST request to get all Registrations");
        return registrationService.findAll();
    }

    /**
     * GET  /registrations/:id : get the "id" registration.
     *
     * @param id the id of the registrationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the registrationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/registrations/{id}")
    @Timed
    public ResponseEntity<RegistrationDTO> getRegistration(@PathVariable String id) {
        log.debug("REST request to get Registration : {}", id);
        RegistrationDTO registrationDTO = registrationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(registrationDTO));
    }

    /**
     * DELETE  /registrations/:id : delete the "id" registration.
     *
     * @param id the id of the registrationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/registrations/{id}")
    @Timed
    public ResponseEntity<Void> deleteRegistration(@PathVariable String id) {
        log.debug("REST request to delete Registration : {}", id);
        registrationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
