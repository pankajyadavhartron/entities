package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.DoctorDTO;
import java.util.List;

/**
 * Service Interface for managing Doctor.
 */
public interface DoctorService {

    /**
     * Save a doctor.
     *
     * @param doctorDTO the entity to save
     * @return the persisted entity
     */
    DoctorDTO save(DoctorDTO doctorDTO);

    /**
     *  Get all the doctors.
     *
     *  @return the list of entities
     */
    List<DoctorDTO> findAll();

    /**
     *  Get the "id" doctor.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DoctorDTO findOne(String id);

    /**
     *  Delete the "id" doctor.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
}
