package com.mycompany.myapp.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Doctor entity.
 */
public class DoctorDTO implements Serializable {

    private UUID id;

    private String enter_test_details;

    private String enter_drug_name;

    @NotNull
    private String quantity;

    @NotNull
    private String frequency;

    private String comment;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEnter_test_details() {
        return enter_test_details;
    }

    public void setEnter_test_details(String enter_test_details) {
        this.enter_test_details = enter_test_details;
    }

    public String getEnter_drug_name() {
        return enter_drug_name;
    }

    public void setEnter_drug_name(String enter_drug_name) {
        this.enter_drug_name = enter_drug_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorDTO doctorDTO = (DoctorDTO) o;
        if(doctorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DoctorDTO{" +
            "id=" + getId() +
            ", enter_test_details='" + getEnter_test_details() + "'" +
            ", enter_drug_name='" + getEnter_drug_name() + "'" +
            ", quantity='" + getQuantity() + "'" +
            ", frequency='" + getFrequency() + "'" +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
