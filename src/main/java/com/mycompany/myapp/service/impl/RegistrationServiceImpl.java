package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.RegistrationService;
import com.mycompany.myapp.domain.Registration;
import com.mycompany.myapp.repository.RegistrationRepository;
import com.mycompany.myapp.service.dto.RegistrationDTO;
import com.mycompany.myapp.service.mapper.RegistrationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Registration.
 */
@Service
public class RegistrationServiceImpl implements RegistrationService{

    private final Logger log = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    private final RegistrationRepository registrationRepository;

    private final RegistrationMapper registrationMapper;

    public RegistrationServiceImpl(RegistrationRepository registrationRepository, RegistrationMapper registrationMapper) {
        this.registrationRepository = registrationRepository;
        this.registrationMapper = registrationMapper;
    }

    /**
     * Save a registration.
     *
     * @param registrationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RegistrationDTO save(RegistrationDTO registrationDTO) {
        log.debug("Request to save Registration : {}", registrationDTO);
        Registration registration = registrationMapper.toEntity(registrationDTO);
        registration = registrationRepository.save(registration);
        return registrationMapper.toDto(registration);
    }

    /**
     *  Get all the registrations.
     *
     *  @return the list of entities
     */
    @Override
    public List<RegistrationDTO> findAll() {
        log.debug("Request to get all Registrations");
        return registrationRepository.findAll().stream()
            .map(registrationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one registration by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public RegistrationDTO findOne(String id) {
        log.debug("Request to get Registration : {}", id);
        Registration registration = registrationRepository.findOne(UUID.fromString(id));
        return registrationMapper.toDto(registration);
    }

    /**
     *  Delete the  registration by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Registration : {}", id);
        registrationRepository.delete(UUID.fromString(id));
    }
}
