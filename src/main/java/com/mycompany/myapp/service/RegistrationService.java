package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.RegistrationDTO;
import java.util.List;

/**
 * Service Interface for managing Registration.
 */
public interface RegistrationService {

    /**
     * Save a registration.
     *
     * @param registrationDTO the entity to save
     * @return the persisted entity
     */
    RegistrationDTO save(RegistrationDTO registrationDTO);

    /**
     *  Get all the registrations.
     *
     *  @return the list of entities
     */
    List<RegistrationDTO> findAll();

    /**
     *  Get the "id" registration.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RegistrationDTO findOne(String id);

    /**
     *  Delete the "id" registration.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
}
