package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.RegistrationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Registration and its DTO RegistrationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RegistrationMapper extends EntityMapper <RegistrationDTO, Registration> {
    
    

}
