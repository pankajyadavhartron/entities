package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Registration;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Registration entity.
 */
@Repository
public class RegistrationRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Registration> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public RegistrationRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Registration.class);
        this.findAllStmt = session.prepare("SELECT * FROM registration");
        this.truncateStmt = session.prepare("TRUNCATE registration");
    }

    public List<Registration> findAll() {
        List<Registration> registrationsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Registration registration = new Registration();
                registration.setId(row.getUUID("id"));
                registration.setFirst_name(row.getString("first_name"));
                registration.setLast_name(row.getString("last_name"));
                registration.setAge(row.getInt("age"));
                registration.setGender(row.getString("gender"));
                registration.setCategoty(row.getString("categoty"));
                registration.setState(row.getString("state"));
                registration.setDistrict(row.getString("district"));
                registration.setAddress(row.getString("address"));
                registration.setRelation_with_patient(row.getString("relation_with_patient"));
                registration.setName_who_bought_patient(row.getString("name_who_bought_patient"));
                registration.setSpeciality(row.getString("speciality"));
                registration.setDoctor_to_visit(row.getString("doctor_to_visit"));
                registration.setRefrel_type(row.getString("refrel_type"));
                registration.setRefered_by(row.getString("refered_by"));
                registration.setRegistration_fee(row.getInt("registration_fee"));
                registration.setUid(row.getInt("uid"));
                registration.setPhone_number(row.getLong("phone_number"));
                registration.setUid_random(row.getString("uid_random"));
                return registration;
            }
        ).forEach(registrationsList::add);
        return registrationsList;
    }

    public Registration findOne(UUID id) {
        return mapper.get(id);
    }

    public Registration save(Registration registration) {
        if (registration.getId() == null) {
            registration.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Registration>> violations = validator.validate(registration);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(registration);
        return registration;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
