package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Doctor;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Doctor entity.
 */
@Repository
public class DoctorRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Doctor> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public DoctorRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Doctor.class);
        this.findAllStmt = session.prepare("SELECT * FROM doctor");
        this.truncateStmt = session.prepare("TRUNCATE doctor");
    }

    public List<Doctor> findAll() {
        List<Doctor> doctorsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Doctor doctor = new Doctor();
                doctor.setId(row.getUUID("id"));
                doctor.setEnter_test_details(row.getString("enter_test_details"));
                doctor.setEnter_drug_name(row.getString("enter_drug_name"));
                doctor.setQuantity(row.getString("quantity"));
                doctor.setFrequency(row.getString("frequency"));
                doctor.setComment(row.getString("comment"));
                return doctor;
            }
        ).forEach(doctorsList::add);
        return doctorsList;
    }

    public Doctor findOne(UUID id) {
        return mapper.get(id);
    }

    public Doctor save(Doctor doctor) {
        if (doctor.getId() == null) {
            doctor.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Doctor>> violations = validator.validate(doctor);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(doctor);
        return doctor;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
