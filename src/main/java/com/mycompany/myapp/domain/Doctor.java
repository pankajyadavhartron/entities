package com.mycompany.myapp.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A Doctor.
 */
@Table(name = "doctor")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;
    @PartitionKey
    private UUID id;

    private String enter_test_details;

    private String enter_drug_name;

    @NotNull
    private String quantity;

    @NotNull
    private String frequency;

    private String comment;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEnter_test_details() {
        return enter_test_details;
    }

    public Doctor enter_test_details(String enter_test_details) {
        this.enter_test_details = enter_test_details;
        return this;
    }

    public void setEnter_test_details(String enter_test_details) {
        this.enter_test_details = enter_test_details;
    }

    public String getEnter_drug_name() {
        return enter_drug_name;
    }

    public Doctor enter_drug_name(String enter_drug_name) {
        this.enter_drug_name = enter_drug_name;
        return this;
    }

    public void setEnter_drug_name(String enter_drug_name) {
        this.enter_drug_name = enter_drug_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public Doctor quantity(String quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFrequency() {
        return frequency;
    }

    public Doctor frequency(String frequency) {
        this.frequency = frequency;
        return this;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getComment() {
        return comment;
    }

    public Doctor comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Doctor doctor = (Doctor) o;
        if (doctor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Doctor{" +
            "id=" + getId() +
            ", enter_test_details='" + getEnter_test_details() + "'" +
            ", enter_drug_name='" + getEnter_drug_name() + "'" +
            ", quantity='" + getQuantity() + "'" +
            ", frequency='" + getFrequency() + "'" +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
