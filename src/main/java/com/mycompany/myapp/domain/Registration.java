package com.mycompany.myapp.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A Registration.
 */
@Table(name = "registration")
public class Registration implements Serializable {

    private static final long serialVersionUID = 1L;
    @PartitionKey
    private UUID id;

    @NotNull
    private String first_name;

    private String last_name;

    @NotNull
    private Integer age;

    @NotNull
    private String gender;

    @NotNull
    private String categoty;

    @NotNull
    private String state;

    @NotNull
    private String district;

    @NotNull
    private String address;

    private String relation_with_patient;

    private String name_who_bought_patient;

    private String speciality;

    private String doctor_to_visit;

    private String refrel_type;

    private String refered_by;

    @NotNull
    private Integer registration_fee;

    @NotNull
    private Integer uid;

    @NotNull
    private Long phone_number;

    private String uid_random;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public Registration first_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Registration last_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Integer getAge() {
        return age;
    }

    public Registration age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public Registration gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCategoty() {
        return categoty;
    }

    public Registration categoty(String categoty) {
        this.categoty = categoty;
        return this;
    }

    public void setCategoty(String categoty) {
        this.categoty = categoty;
    }

    public String getState() {
        return state;
    }

    public Registration state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public Registration district(String district) {
        this.district = district;
        return this;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public Registration address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRelation_with_patient() {
        return relation_with_patient;
    }

    public Registration relation_with_patient(String relation_with_patient) {
        this.relation_with_patient = relation_with_patient;
        return this;
    }

    public void setRelation_with_patient(String relation_with_patient) {
        this.relation_with_patient = relation_with_patient;
    }

    public String getName_who_bought_patient() {
        return name_who_bought_patient;
    }

    public Registration name_who_bought_patient(String name_who_bought_patient) {
        this.name_who_bought_patient = name_who_bought_patient;
        return this;
    }

    public void setName_who_bought_patient(String name_who_bought_patient) {
        this.name_who_bought_patient = name_who_bought_patient;
    }

    public String getSpeciality() {
        return speciality;
    }

    public Registration speciality(String speciality) {
        this.speciality = speciality;
        return this;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctor_to_visit() {
        return doctor_to_visit;
    }

    public Registration doctor_to_visit(String doctor_to_visit) {
        this.doctor_to_visit = doctor_to_visit;
        return this;
    }

    public void setDoctor_to_visit(String doctor_to_visit) {
        this.doctor_to_visit = doctor_to_visit;
    }

    public String getRefrel_type() {
        return refrel_type;
    }

    public Registration refrel_type(String refrel_type) {
        this.refrel_type = refrel_type;
        return this;
    }

    public void setRefrel_type(String refrel_type) {
        this.refrel_type = refrel_type;
    }

    public String getRefered_by() {
        return refered_by;
    }

    public Registration refered_by(String refered_by) {
        this.refered_by = refered_by;
        return this;
    }

    public void setRefered_by(String refered_by) {
        this.refered_by = refered_by;
    }

    public Integer getRegistration_fee() {
        return registration_fee;
    }

    public Registration registration_fee(Integer registration_fee) {
        this.registration_fee = registration_fee;
        return this;
    }

    public void setRegistration_fee(Integer registration_fee) {
        this.registration_fee = registration_fee;
    }

    public Integer getUid() {
        return uid;
    }

    public Registration uid(Integer uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Long getPhone_number() {
        return phone_number;
    }

    public Registration phone_number(Long phone_number) {
        this.phone_number = phone_number;
        return this;
    }

    public void setPhone_number(Long phone_number) {
        this.phone_number = phone_number;
    }

    public String getUid_random() {
        return uid_random;
    }

    public Registration uid_random(String uid_random) {
        this.uid_random = uid_random;
        return this;
    }

    public void setUid_random(String uid_random) {
        this.uid_random = uid_random;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Registration registration = (Registration) o;
        if (registration.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), registration.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Registration{" +
            "id=" + getId() +
            ", first_name='" + getFirst_name() + "'" +
            ", last_name='" + getLast_name() + "'" +
            ", age='" + getAge() + "'" +
            ", gender='" + getGender() + "'" +
            ", categoty='" + getCategoty() + "'" +
            ", state='" + getState() + "'" +
            ", district='" + getDistrict() + "'" +
            ", address='" + getAddress() + "'" +
            ", relation_with_patient='" + getRelation_with_patient() + "'" +
            ", name_who_bought_patient='" + getName_who_bought_patient() + "'" +
            ", speciality='" + getSpeciality() + "'" +
            ", doctor_to_visit='" + getDoctor_to_visit() + "'" +
            ", refrel_type='" + getRefrel_type() + "'" +
            ", refered_by='" + getRefered_by() + "'" +
            ", registration_fee='" + getRegistration_fee() + "'" +
            ", uid='" + getUid() + "'" +
            ", phone_number='" + getPhone_number() + "'" +
            ", uid_random='" + getUid_random() + "'" +
            "}";
    }
}
