(function() {
    'use strict';

    angular
        .module('hshrcApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
