(function() {
    'use strict';

    angular
        .module('hshrcApp')
        .controller('RegistrationDetailController', RegistrationDetailController);

    RegistrationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Registration'];

    function RegistrationDetailController($scope, $rootScope, $stateParams, previousState, entity, Registration) {
        var vm = this;

        vm.registration = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hshrcApp:registrationUpdate', function(event, result) {
            vm.registration = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
