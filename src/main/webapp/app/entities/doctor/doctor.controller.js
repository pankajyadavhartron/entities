(function() {
    'use strict';

    angular
        .module('hshrcApp')
        .controller('DoctorController', DoctorController);

    DoctorController.$inject = ['Doctor'];

    function DoctorController(Doctor) {

        var vm = this;

        vm.doctors = [];

        loadAll();

        function loadAll() {
            Doctor.query(function(result) {
                vm.doctors = result;
                vm.searchQuery = null;
            });
        }
    }
})();
